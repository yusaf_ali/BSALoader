module me.yusaf.opengl.loader.bsa {
	exports bsa;

	requires me.yusaf.opengl.logger;
	requires me.yusaf.opengl.settings;
	requires org.apache.commons.compress;
}